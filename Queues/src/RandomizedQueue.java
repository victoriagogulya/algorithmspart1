import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] items;
    private int length;
    // construct an empty randomized queue
    public RandomizedQueue() {
        length = 0;
        items = (Item[]) new Object[1];
    }
    // is the queue empty?
    public boolean isEmpty() {
        return length == 0;
    }
    // return the number of items on the queue
    public int size() {
        return length;
    }
    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (length == items.length) {
            resize(length * 2);
        }
        items[length++] = item;
    }
    // remove and return a random item
    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int indexRandom = StdRandom.uniform(length);
        Item tmp = items[indexRandom];
        items[indexRandom] = items[length - 1];
        items[--length] = null;
        if (length > 0 && length == items.length / 4)
            resize(items.length / 2);
        return tmp;
    }
    // return (but do not remove) a random item
    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return items[StdRandom.uniform(length)];
    }
    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private int current;
        private Item[] arr;

        RandomizedQueueIterator() {
            current = 0;
            arr = (Item[]) new Object[length];
            for (int i = 0; i < length; i++)
                arr[i] = items[i];
            StdRandom.shuffle(arr);
        }

        @Override
        public boolean hasNext() {
            return current != length;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return arr[current++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    private void resize(int capacity) {
        Item[] itemsTemp = (Item[]) new Object[capacity];
        for (int i = 0; i < length; i++)
            itemsTemp[i] = items[i];
        items = itemsTemp;
    }

    // unit testing
    public static void main(String[] args) {

    }
}
