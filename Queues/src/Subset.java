
public class Subset {
    public static void main(String[] args) {
        if (args.length == 1) {
            int k = Integer.parseInt(args[0]);
            RandomizedQueue<String> queue = new RandomizedQueue<>();

            while (!StdIn.isEmpty()) {
                String str = StdIn.readString();
                queue.enqueue(str);
            }

            for (int i = 0; i < k; i++) {
                String str = queue.dequeue();
                StdOut.println(str);
            }
        }
    }
}
