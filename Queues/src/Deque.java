import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private Node first;
    private Node last;
    private int size;

    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }
    // construct an empty deque
    public Deque() {
    }
    // is the deque empty?
    public boolean isEmpty() {
        return size == 0;
    }
    // return the number of items on the deque
    public int size() {
        return size;
    }
    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        Node oldItem = first;
        first = new Node();
        first.item = item;
        first.next = oldItem;
        if (isEmpty()) {
            last = first;
        }
        else {
            oldItem.previous = first;
        }
        size++;
    }
    // add the item to the end
    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        Node oldItem = last;
        last = new Node();
        last.item = item;
        last.previous = oldItem;
        if (!isEmpty()) {
            oldItem.next = last;
        }
        else {
            first = last;
        }
        size++;
    }
    // remove and return the item from the front
    public Item removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item item = first.item;
        if (size() == 1) {
            first = null;
            last = null;
        }
        else {
            first = first.next;
            first.previous = null;
        }
        size--;
        return item;
    }
    // remove and return the item from the end
    public Item removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        Item item = last.item;
        if (size() == 1) {
            first = null;
            last = null;
        }
        else {
            last = last.previous;
            last.next = null;
        }
        size--;
        return item;
    }
    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item item = current.item;
            current = current.next;
            return item;
        }
    }
    // unit testing
    public static void main(String[] args) {
    }
}
