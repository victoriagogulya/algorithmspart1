import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;


public class Fast {
    private static ArrayList<String> segments = new ArrayList<>();

    private static Point[] getPointsFromFile(String fileName) {
        In in = new In(fileName);
        Point[] points = new Point[in.readInt()];
        int x;
        int y;
        for (int i = 0; i < points.length; i++) {
            x = in.readInt();
            y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        return points;
    }

    private static void printLineSegment(ArrayList<Point> list) {
        Collections.sort(list);
        StringBuilder b = new StringBuilder();

        for (int a = 0; a < list.size(); a++) {
            b.append(list.get(a).toString());
            if (a != list.size() - 1) {
                b.append(" -> ");
            }
        }
        b.append("\n");
        String str = b.toString();
        if (!segments.contains(str)) {
            segments.add(str);
            StdOut.println(str);
            list.get(0).drawTo(list.get(list.size() - 1));
        }
    }

    public static void main(String[] args) {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);

        Point[] pointsOriginal = getPointsFromFile(args[0]);
        Point[] points = Arrays.copyOf(pointsOriginal, pointsOriginal.length);


        ArrayList<Point> list = new ArrayList<>();
        for (int i = 0; i < pointsOriginal.length; i++) {
            Arrays.sort(points, pointsOriginal[i].SLOPE_ORDER);

            int index = 0;
            int j = 1;
            for (; j < points.length; j++) {
                if (pointsOriginal[i].SLOPE_ORDER.compare(points[index], points[j]) == 0) {
                    list.add(points[j]);
                }
                else {
                    if (list.size() >= 3) {
                        list.add(pointsOriginal[i]);
                        printLineSegment(list);
                    }
                    list.clear();
                    list.add(points[j]);
                    index = j;
                }
            }
            if (list.size() >= 3) {
                list.add(pointsOriginal[i]);
                printLineSegment(list);
            }
            list.clear();
        }
    }
}
