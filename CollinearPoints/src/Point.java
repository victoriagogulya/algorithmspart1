import java.util.Comparator;

public class Point implements Comparable<Point> {
    // compare points by slope to this point
    public final Comparator<Point> SLOPE_ORDER = new SlopeOrder();
    private final int x;
    private final int y;
    // construct the point (x, y)
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    // draw this point
    public void draw() {
        StdDraw.point(x, y);
    }
    // draw the line segment from this point to that point
    public void drawTo(Point that) {
        StdDraw.line(this.x, this.y, that.x, that.y);
    }
    // string representation
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
    // is this point lexicographically smaller than that point?
    public int compareTo(Point that) {
        if (y < that.y || y == that.y && x < that.x) return -1;
        if (y == that.y && x == that.x) return 0;
        return 1;
    }
    // the slope between this point and that point
    public double slopeTo(Point that) {
        if (y == that.y && x == that.x) return Double.NEGATIVE_INFINITY;
        if (y == that.y) return 0.0;
        if (x == that.x) return Double.POSITIVE_INFINITY;
        return (double) (that.y - y) / (double) (that.x - x);
    }

    private class SlopeOrder implements Comparator<Point> {
        @Override
        public int compare(Point p1, Point p2) {
            double slopeP1 = slopeTo(p1);
            double slopeP2 = slopeTo(p2);
            if (slopeP1 < slopeP2) return -1;
            if (slopeP1 > slopeP2) return 1;
            return 0;
        }
    }
}
