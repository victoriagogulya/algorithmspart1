
public class Brute {

    private static Point[] getPointsFromFile(String fileName) {
        In in = new In(fileName);
        Point[] points = new Point[in.readInt()];
        int x;
        int y;
        for (int i = 0; i < points.length; i++) {
            x = in.readInt();
            y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        return points;
    }

    private static void searchLineSegments(Point[] points) {
        for (int p = 0; p < points.length; p++) {
            for (int q = p + 1; q < points.length; q++) {
                if (points[p].compareTo(points[q]) != 0) {
                    for (int r = q + 1; r < points.length; r++) {
                        if (points[q].compareTo(points[r]) != 0
                                && points[p].slopeTo(points[q]) == points[q].slopeTo(points[r])) {
                            for (int s = r + 1; s < points.length; s++) {
                                if (points[r].compareTo(points[s]) != 0
                                        && points[q].slopeTo(points[r]) == points[r].slopeTo(points[s])) {

                                    StdOut.println(points[p].toString() + " -> "
                                            + points[q].toString() + " -> "
                                            + points[r].toString() + " -> " + points[s].toString());
                                    points[p].drawTo(points[s]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);

        Point[] points = getPointsFromFile(args[0]);
        Merge.sort(points);
        searchLineSegments(points);
    }
}
