import java.util.Stack;


public class Solver {

    private Node node;
    private boolean solvable;
    private int movesCount;


    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null) {
            throw new NullPointerException();
        }
        node = new Node(initial, null, 0);
        Node nodeTwin = new Node(initial.twin(), null, 0);
        MinPQ<Node> minPQ = new MinPQ<>();
        MinPQ<Node> minPQTwin = new MinPQ<>();

        if (node.board.isGoal()) {
            solvable = true;
        }

        Iterable<Board> nodeNeighbors = null;
        Iterable<Board> nodeNeighborsTwin = null;

        while (!node.board.isGoal() && !nodeTwin.board.isGoal()) {
            nodeNeighbors = node.board.neighbors();
            for (Board board : nodeNeighbors) {
                if (node.previous == null || !board.equals(node.previous.board)) {
                    minPQ.insert(new Node(board, node, node.moves + 1));
                }
            }
            node = minPQ.delMin();

            nodeNeighborsTwin = nodeTwin.board.neighbors();
            for (Board board : nodeNeighborsTwin) {
                if (nodeTwin.previous == null || !board.equals(nodeTwin.previous.board)) {
                    minPQTwin.insert(new Node(board, nodeTwin, nodeTwin.moves + 1));
                }
            }
            nodeTwin = minPQTwin.delMin();
        }

        movesCount = node.moves;
        if (node.board.isGoal()) {
            solvable = true;
        }
    }

    // is the initial board solvable?
    public boolean isSolvable() {
        return solvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        if (!solvable) {
            movesCount = -1;
        }
        return movesCount;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!solvable) return null;
        else {
            Stack<Board> stack = new Stack<>();
            while (node != null) {
                stack.push(node.board);
                node = node.previous;
            }
            return stack;
        }
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

    private class Node implements Comparable<Node> {
        private final Board board;
        private final Node previous;
        private int moves;

        Node(Board board, Node previous, int moves) {
            this.board = board;
            this.previous = previous;
            this.moves = moves;
        }

        @Override
        public int compareTo(Node o) {
            int result = (this.board.manhattan() + this.moves) - (o.board.manhattan() + o.moves);
            if (result < 0) return -1;
            if (result > 0) return 1;
            return 0;
        }
    }
}
