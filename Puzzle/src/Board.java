import java.util.Arrays;
import java.util.Stack;

public class Board {

    private int[][] board;
    private int zeroX;
    private int zeroY;
    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        board = new int[blocks.length][blocks.length];
        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks.length; j++) {
                if (blocks[i][j] == 0) {
                    zeroY = i;
                    zeroX = j;
                }
                board[i][j] = blocks[i][j];
            }
        }
    }
    // board dimension N
    public int dimension() {
        return board.length;
    }
    // number of blocks out of place
    public int hamming() {
        int count = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] != i * board.length + j + 1) {
                    count++;
                }
            }
        }
       if (board[zeroY][zeroX] == 0) {
           count--;
       }
        return count;
    }
    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int sum = 0;
        int y = 0;
        int x = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (i == zeroY && j == zeroX) {
                    y = board.length - 1;
                    x = board.length - 1;
                }
                else {
                    y = (board[i][j] - 1) / board.length;
                    x = board[i][j] - y * board.length - 1;
                }
                sum += (Math.abs(i - y) + Math.abs(j - x));
            }
        }
        return sum;
    }
    // is this board the goal board?
    public boolean isGoal() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (i == board.length - 1 && j == board.length - 1) {
                    if (zeroY != i || zeroX != j) return false;
                }
                else if (board[i][j] != i * board.length + j + 1) {
                    return false;
                }
            }
        }
        return true;
    }
    // a board that is obtained by exchanging two adjacent blocks in the same row
    public Board twin() {
        int row = 0;
        int[][] twin = copyArray();
        if (twin[0][0] == 0 || twin[0][1] == 0) {
            row = 1;
        }
        int tmp = 0;
        tmp = twin[row][0];
        twin[row][0] = twin[row][1];
        twin[row][1] = tmp;
        return new Board(twin);
    }
    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;
        return Arrays.deepEquals(that.board, this.board);
    }
    // all neighboring boards
    public Iterable<Board> neighbors() {
        Stack<Board> stack = new Stack<>();
        if (zeroY > 0) {
            int[][] array = copyArray();
            int tmp = array[zeroY][zeroX];
            array[zeroY][zeroX] = array[zeroY - 1][zeroX];
            array[zeroY - 1][zeroX] = tmp;
            stack.push(new Board(array));
        }
        if (zeroY < board.length - 1) {
            int[][] array = copyArray();
            int tmp = array[zeroY][zeroX];
            array[zeroY][zeroX] = array[zeroY + 1][zeroX];
            array[zeroY + 1][zeroX] = tmp;
            stack.push(new Board(array));
        }
        if (zeroX > 0) {
            int[][] array = copyArray();
            int tmp = array[zeroY][zeroX];
            array[zeroY][zeroX] = array[zeroY][zeroX - 1];
            array[zeroY][zeroX - 1] = tmp;
            stack.push(new Board(array));
        }
        if (zeroX < board.length - 1) {
            int[][] array = copyArray();
            int tmp = array[zeroY][zeroX];
            array[zeroY][zeroX] = array[zeroY][zeroX + 1];
            array[zeroY][zeroX + 1] = tmp;
            stack.push(new Board(array));
        }
        return stack;
    }
    // string representation of this board (in the output format specified below)
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(board.length + "\n");
        for (int[] subArray : board) {
            for (int value : subArray) {
                builder.append(String.format("%2d ", value));
            }
            builder.append("\n");
        }
        return builder.toString();
    }
    // unit tests (not graded)
    public static void main(String[] args) {
        int[][] arr = new int[][] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 0}
        };
        Board b = new Board(arr);
        if (b.isGoal()) {
            StdOut.println(b.toString());
        }

    }

    private int[][] copyArray() {
        int[][] tmp = new int[board.length][board.length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                tmp[i][j] = board[i][j];
            }
        }
        return tmp;
    }
}
