
public class Percolation {
    private final boolean[][] gridState;
    private final int size;
    private int top;
    private int bottom;
    private WeightedQuickUnionUF tree;
    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        size = N;
        top = size * size;
        bottom = top + 1;
        gridState = new boolean[size][size];
        tree = new WeightedQuickUnionUF(size * size + 2);
    }

    private int gridIndexesToSite(int i, int j) {
        return ((i - 1) + (j - 1) * size);
    }

    private void connectToNorth(int i, int j) {
        if (i == 1)
            tree.union(top, gridIndexesToSite(i, j));
        else if (isOpen(i - 1, j))
            tree.union(gridIndexesToSite(i - 1, j), gridIndexesToSite(i, j));
    }

    private void connectToSouth(int i, int j) {
        if (i == size)
            tree.union(bottom, gridIndexesToSite(i, j));
        else if (isOpen(i + 1, j))
            tree.union(gridIndexesToSite(i + 1, j), gridIndexesToSite(i, j));
    }

    private void connectToWest(int i, int j) {
        if (j != 1 && isOpen(i, j - 1))
            tree.union(gridIndexesToSite(i, j - 1), gridIndexesToSite(i, j));
    }

    private void connectToEast(int i, int j) {
        if (j != size && isOpen(i, j + 1))
            tree.union(gridIndexesToSite(i, j + 1), gridIndexesToSite(i, j));
    }
    // open site (row i, column j) if it is not open already
    public void open(int i, int j) {
        if (!isOpen(i, j)) {
            gridState[i - 1][j - 1] = true;
            connectToNorth(i, j);
            connectToSouth(i, j);
            connectToWest(i, j);
            connectToEast(i, j);
        }
    }
    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        if (i < 1 || j < 1 || i > size || j > size)
            throw new IndexOutOfBoundsException("Index out of bounds exception");
        return gridState[i - 1][j - 1];
    }
    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        return (isOpen(i, j) && tree.connected(top, gridIndexesToSite(i, j)));
    }
    // does the system percolate?
    public boolean percolates() {
        return tree.connected(top, bottom);
    }
    // test client (optional)
    public static void main(String[] args)
    {

    }
}