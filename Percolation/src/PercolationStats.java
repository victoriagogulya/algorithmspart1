
public class PercolationStats {
    private double[] results;

    // perform T independent experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) throw new IllegalArgumentException();
        results = new double[T];
        for (int i = 0; i < T; i++) {
            Percolation perc = new Percolation(N);
            int steps = 0;
            while(!perc.percolates()) {
                int row = StdRandom.uniform(N) + 1;
                int column = StdRandom.uniform(N) + 1;
                if (!perc.isOpen(row, column)) {
                    perc.open(row, column);
                    steps++;
                }
            }
            results[i] = (double)steps / (N * N);
        }
    }
    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(results);
    }
    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(results);
    }
    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - ((1.96 * stddev()) / Math.sqrt(results.length));
    }
    // high endpoint of 95% confidence interval
    public double confidenceHi()
    {
        return mean() + ((1.96 * stddev()) / Math.sqrt(results.length));
    }
    // test client (described below)
    public static void main(String[] args)
    {
        if (args.length == 2)
        {
            PercolationStats perlocationSt = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
            StdOut.print("mean = " + perlocationSt.mean() + "\n");
            StdOut.print("std dev = " + perlocationSt.stddev() + "\n");
            StdOut.print("95% confidence interval = " + perlocationSt.confidenceLo() + ", " + perlocationSt.confidenceHi());
        }
        else
            StdOut.print("Enter size and count of runs.");
    }
}