import java.util.Set;
import java.util.TreeSet;

public class PointSET {

    private Set<Point2D> set = new TreeSet<>();

    // construct an empty set of points
    public PointSET() { }

    // is the set empty?
    public boolean isEmpty() { return set.isEmpty(); }

    // number of points in the set
    public int size() { return set.size(); }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (!contains(p)) set.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) throw new NullPointerException();
        return set.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        for (Point2D point : set) {
            point.draw();
        }
    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new NullPointerException();

        Stack<Point2D> stack = new Stack<>();
        for (Point2D point : set) {
            if (rect.contains(point)) {
                stack.push(point);
            }
        }
        return stack;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) throw new NullPointerException();
        Point2D nearestPoint = null;
        if (!set.isEmpty()) {
            nearestPoint = set.iterator().next();
            for (Point2D currentPoint : set) {
                if (p.distanceSquaredTo(currentPoint) < p.distanceSquaredTo(nearestPoint)) {
                    nearestPoint = currentPoint;
                }
            }
        }
        return nearestPoint;
    }

    public static void main(String[] args) { }
}
