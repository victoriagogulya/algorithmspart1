
public class KdTree {

    private Node root;
    private int size;

    // construct an empty set of points
    public KdTree() { }

    // is the set empty?
    public boolean isEmpty() { return size == 0; }

    // number of points in the set
    public int size() { return size; }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (p == null) throw  new NullPointerException();
        if (root == null) {
            root = new Node();
            root.vertical = true;
            root.point2D = p;
            root.rect = new RectHV(0.0, 0.0, 1.0, 1.0);
            size++;
        }
        else {
            root = insert(null, root, p);
        }
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) throw  new NullPointerException();
        return get(root, p) != null;
    }

    // draw all points to standard draw
    public void draw() {
        draw(root);
    }

    // all points that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new NullPointerException();
        Stack<Point2D> stack = new Stack<>();
        range(root, rect, stack);
        return stack;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) throw new NullPointerException();
        Point2D nearestPoint = null;
        if (root != null) {
            nearestPoint = root.point2D;
            nearest(root, p, nearestPoint);
        }
        return nearestPoint;
    }

    public static void main(String[] args) { }

    private class Node {
        private Point2D point2D;
        private boolean vertical;
        private RectHV rect;
        private Node nodeLeft;
        private Node nodeRight;
    }

    private Node get(Node node, Point2D p) {
        if (node == null) return null;
        if (node.point2D.equals(p)) return node;
        if (node.vertical) {
            if (p.x() < node.point2D.x()) return get(node.nodeLeft, p);
            else return get(node.nodeRight, p);
        }
        else {
            if (p.y() < node.point2D.y()) return get(node.nodeLeft, p);
            else return get(node.nodeRight, p);
        }
    }

    private Node insert(Node parent, Node node, Point2D p) {
        if (node == null) {
            node = new Node();
            node.point2D = p;
            node.vertical = !parent.vertical;
            if (parent.vertical) {
                if (p.x() < parent.point2D.x())
                    node.rect = new RectHV(parent.rect.xmin(), parent.rect.ymin(), parent.point2D.x(), parent.rect.ymax());
                else
                    node.rect = new RectHV(parent.point2D.x(), parent.rect.ymin(), parent.rect.xmax(), parent.rect.ymax());
            }
            else {
                if (p.y() < parent.point2D.y())
                    node.rect = new RectHV(parent.rect.xmin(), parent.rect.ymin(), parent.rect.xmax(), parent.point2D.y());
                else
                    node.rect = new RectHV(parent.rect.xmin(), parent.point2D.y(), parent.rect.xmax(), parent.rect.ymax());
            }
            size++;
        }
       else if (!node.point2D.equals(p)) {
            if (node.vertical) {
                if (p.x() < node.point2D.x()) node.nodeLeft = insert(node, node.nodeLeft, p);
                else node.nodeRight = insert(node, node.nodeRight, p);
            }
            else {
                if (p.y() < node.point2D.y()) node.nodeLeft = insert(node, node.nodeLeft, p);
                else node.nodeRight = insert(node, node.nodeRight, p);
            }
        }
        return node;
    }

    private void range(Node node, RectHV rect, Stack<Point2D> stack) {
        if (node != null && node.rect.intersects(rect)) {
            if (rect.contains(node.point2D)) stack.push(node.point2D);
            range(node.nodeLeft, rect, stack);
            range(node.nodeRight, rect, stack);
        }
    }

    private void nearest(Node node, Point2D p, Point2D nearestPoint) {
        if (p.distanceSquaredTo(node.point2D) < p.distanceSquaredTo(nearestPoint)) {
                nearestPoint = node.point2D;
        }

        if (node.nodeLeft != null && node.nodeLeft.rect.contains(p)) {
            nearest(node.nodeLeft, p, nearestPoint);
            if (node.nodeRight != null && p.distanceSquaredTo(nearestPoint)
                    > node.nodeRight.rect.distanceSquaredTo(p)) {
                nearest(node.nodeRight, p, nearestPoint);
            }
        }
        else if (node.nodeRight != null && node.nodeRight.rect.contains(p)) {
            nearest(node.nodeRight, p, nearestPoint);
            if (node.nodeLeft != null && p.distanceSquaredTo(nearestPoint)
                    > node.nodeLeft.rect.distanceSquaredTo(p)) {
                nearest(node.nodeLeft, p, nearestPoint);
            }
        }
    }

    private void draw(Node node) {
        if (node != null) {
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(.01);
            node.point2D.draw();

            if (node.vertical) {
                StdDraw.setPenRadius(.003);
                StdDraw.setPenColor(StdDraw.RED);

                StdDraw.line(node.point2D.x(), node.rect.ymin(), node.point2D.x(), node.rect.ymax());
            } else {
                StdDraw.setPenRadius(.003);
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.line(node.rect.xmin(), node.point2D.y(), node.rect.xmax(), node.point2D.y());
            }

            draw(node.nodeLeft);
            draw(node.nodeRight);
        }
    }
}
